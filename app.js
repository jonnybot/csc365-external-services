"use strict";

//Imports, modules, libraries, etc.
const express = require('express'),
    hoffman = require('hoffman'),
    path = require('path'),
    passport = require('passport'),
    TwitterStrategy = require('passport-twitter').Strategy,
    bodyParser = require('body-parser'),
    authConfig = require('./modules/authConfig'),
    authHandling = require('./modules/authHandling'),
    https = require('https')
;

const app = express();
//Wire up the view engine
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'dust');
app.engine('dust', hoffman.__express());

const listeningPort = parseInt(process.argv[2]) || 3000;

//Wire up the body parsers for if/when we want to add POST requests to this application
app.use(bodyParser.urlencoded({
    extended: false
}));
app.use(bodyParser.json());

//Add the express-session middleware so we can use sessions & cookies
app.use(require('express-session')({
    secret: 'sooooper secretive secret of secrecy',
    resave: true,
    saveUninitialized: true
}));

//Initialize passport for authentication
app.use(passport.initialize());
app.use(passport.session());

//Configure the Twitter authentication strategy
let twitterOptions = {
    consumerKey: authConfig.TWITTER_CONSUMER_KEY,
    consumerSecret: authConfig.TWITTER_CONSUMER_SECRET,
    callbackURL: "http://127.0.0.1:3000/auth/twitter/callback"
};

let twitterAuthStrat = 'twitter-auth';
passport.use(twitterAuthStrat, new TwitterStrategy(twitterOptions,
    (token, tokenSecret, profile, done) => {
        //token and tokenSecret we'll discuss a bit more on Wednesday
        //asdfsdafda
        done(null, {
            profile: profile,
            token: token,
            tokenSecret: tokenSecret
        });
    }
));

/* Configure Passport authenticated session persistence.

 In order to restore authentication state across HTTP requests, Passport needs to serialize users into and deserialize users out of the session.  In a production-quality application, this would typically be as simple as supplying the user ID when serializing, and querying the user record by ID from the database when deserializing.  However, due to the fact that this example does not have a database, the complete Twitter profile is serialized and deserialized.

 tl;dr: Don't do this IRL -- at most, store only a unique user identifier in the session
*/
passport.serializeUser(function (user, cb) {
    cb(null, user);
});

passport.deserializeUser(function (obj, cb) {
    cb(null, obj);
});


app.get('/', (request, response) => {
    response.render('index', {});
});

// Redirect the user to Twitter for authentication.  When complete, Twitter
// will redirect the user back to the application at /auth/twitter/callback
app.get('/auth/twitter', passport.authenticate(twitterAuthStrat));

// Twitter will redirect the user to this URL after approval.  Finish the
// authentication process by attempting to obtain an access token.  If
// access was granted, the user will be logged in.  Otherwise,
// authentication has failed.
app.get('/auth/twitter/callback', passport.authenticate(twitterAuthStrat, {
    successRedirect: '/',
    failureRedirect: '/'
}));

//Okay, let's add an endpoint that requires the user to be authenticated
app.get('/protected',
    (request, response) => {
        if (request.user) { //check that they're logged in...
            response.render('protected', request.user.profile._json);
        } else { //If not, we'll send them back to the login page
            response.redirect('/auth/twitter');
        }
    }
);

app.get('/auth/logout', (request, response) => {
    request.logout();
    response.redirect('/');
});

let searchOptions = {
    hostname: 'api.twitter.com',
    port: 443,
    path: '/1.1/search/tweets.json',
    method: 'GET',
};

app.get('/hashtag/:whatevs', (request, response) => {
    let user = request.user;

    if (user) { //check that they're logged in...
        let hashTag = request.params.whatevs; //Note the use of route parameters to accept any hashtag https://expressjs.com/en/guide/routing.html#route-parameters

        let searchSpecificOptions = Object.assign(searchOptions, {}); //clone the base object so we have a new one on every request

        searchSpecificOptions.path += `?q=${hashTag}`; //Add the search parameter to our request as a query string

        searchSpecificOptions['headers'] = authHandling.getOauthHeader(authConfig.TWITTER_CONSUMER_KEY,
            authConfig.TWITTER_CONSUMER_SECRET,
            user.token,
            user.tokenSecret,
            hashTag); //Add a properly formatted Oauth 1.0 Authorization header to the request

        //Build the HTTP request to Twitter and handle their response
        let requestToTwitter = https.request(searchSpecificOptions, (responseFromTwitter) => {
            let allTweets;
            responseFromTwitter.on('data', (tweets) => {
                if (allTweets) {
                    allTweets += tweets;
                } else {
                    allTweets = tweets;
                }
            });
            responseFromTwitter.on('error', (err) => {
                console.error(err);
            });
            responseFromTwitter.on('end', () => {
                let parsedTweets = JSON.parse(allTweets.toString());
                response.render('hashtag', parsedTweets);
            });
        });
        requestToTwitter.end(); //We always have to explicitly end the request

    } else { //If not, we'll send them back to the login page
        response.redirect('/auth/twitter');
    }
});

app.listen(listeningPort, () => {
    console.log(`My app is listening on port ${listeningPort}!`);
});
